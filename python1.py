from functools import total_ordering

## there are 20 amino acids 
## sequence
amino_acid_names = [
    "A",
    "G",
    "M",
    "L",
    "H",
    "R",
    "N",
    "D",
    "T",
    "F",
    "W",
    "K",
    "Q",
    "E",
    "S",
    "P",
    "V",
    "C",
    "I",
    "Y",
]


## lets us load the fasta file and read the
## contents


def load_fasta(path):

    proteinfasta = open(path).read()
    # fasta file is a string
    # split the file along >sp
    aminoacids = proteinfasta.split(">sp")
    aminoacids = aminoacids[1:]

    fasta_list = []
    try:
        for aa in aminoacids:
            fasta_list.append(Protein(aa))

        return fasta_list
    except:
        raise ValueError("it is not a .fasta file")


## using total_ordering
@total_ordering
class Protein:
    def __init__(self, protein):
        header = protein.split("\n")[0]

        fullseq = protein.split("\n")[1 : len(protein.split("\n")) - 1]

        self.seq = "".join(fullseq)
        self.size = len(self.seq)

        self.id = header.split("|")[1]
        fastalistsequence = header.split("|")[2]

        ind_os = fastalistsequence.find("OS=")
        ind_ox = fastalistsequence.find("OX=")
        ind_pe = fastalistsequence.find("PE=")
        ind_sv = fastalistsequence.find("SV=")
        ind_gn = fastalistsequence.find("GN=")

        self.name = fastalistsequence[:ind_os].strip()
        self.os = fastalistsequence[ind_os + 3 : ind_ox]
        self.gn = fastalistsequence[ind_gn + 3 : ind_pe].strip()
        self.ox = int(fastalistsequence[ind_ox + 3 : ind_gn])
        self.pe = int(fastalistsequence[ind_pe + 3 : ind_sv])
        self.sv = int(fastalistsequence[ind_sv + 3 :])

        self.aminoacidcounts = {}

        for acids in amino_acid_names:
            self.aminoacidcounts[acids] = self.seq.count(acids)

    def __eq__(self, ink):
        return self.size == ink.size

    def __lt__(self, ink):
        return self.size < ink.size

    def __ne__(slef, ink):
        return not self == ink

    def __repr__(self):
        return "{} id: {}".format(self.name, self.id)


def sort_proteins_pe(proteins):
    return sorted(proteins, key=lambda prote: prote.pe, reverse=True)


def sort_proteins_aa(proteins, aminacid):
    if aminacid not in amino_acid_names:
        raise ValueError("not an aminoacid")
    return sorted(proteins, key=lambda prote: prote.aminoacidcounts[aminacid], reverse=True)


def find_protein_with_motif(proteins, motif):

    pro_motif = []
    for aproteins in proteins:
        if aproteins.seq.find(motif) != -1:
            pro_motif.append(aproteins)

    return pro_motif

